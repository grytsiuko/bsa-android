package com.example.bsa_android.model

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface PostDao {
    @Query("SELECT * FROM posts")
    fun getAll(): LiveData<List<Post>>

    @Query("SELECT * FROM posts")
    fun getAllNow(): List<Post>

    @Query("SELECT * FROM posts WHERE id = :id")
    fun getById(id: String): Post?

    @Insert
    suspend fun insert(post: Post)

    @Query("DELETE FROM posts")
    suspend fun deleteAll()
}