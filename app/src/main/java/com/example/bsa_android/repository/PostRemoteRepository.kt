package com.example.bsa_android.repository

import com.example.bsa_android.model.Post
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL

class PostRemoteRepository {
    val link = "https://my-json-server.typicode.com/grytsiuko/posts-json/posts"

    class Response(json: String) : JSONArray(json) {
        val data: List<Post> = this
            .let { 0.until(it.length()).map { i -> it.optJSONObject(i) } }
            .map { JSONObject(it.toString()) }
            .map {
                Post(
                    it.optString("id"),
                    it.optString("title"),
                    it.optString("nickname"),
                    it.optString("fullname"),
                    it.optString("date"),
                    it.optString("text")
                )
            }
    }

    fun getAll(): List<Post> {
        val url = URL(link)

        with(url.openConnection() as HttpURLConnection) {
            responseCode
            val responseString = String(inputStream.readBytes())
            return Response(responseString).data
        }
    }

}