package com.example.bsa_android.repository

import androidx.lifecycle.LiveData
import com.example.bsa_android.model.Post
import com.example.bsa_android.model.PostDao

class PostLocalRepository(private val postDao: PostDao) {

    fun getAll(): LiveData<List<Post>> {
        return postDao.getAll()
    }

    fun getAllNow(): List<Post> {
        return postDao.getAllNow()
    }

    fun getById(id: String): Post? {
        return postDao.getById(id)
    }

    suspend fun insert(post: Post) {
        postDao.insert(post)
    }

    suspend fun deleteAll() {
        postDao.deleteAll()
    }

}