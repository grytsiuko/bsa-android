package com.example.bsa_android

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bsa_android.recyclerView.PostListAdapter
import com.example.bsa_android.databinding.FragmentSavedPostsBinding
import com.example.bsa_android.model.Post
import com.example.bsa_android.viewModel.SavedPostsViewModel

class SavedPostsFragment : Fragment() {

    lateinit var viewModel: SavedPostsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProviders.of(this).get(SavedPostsViewModel::class.java)

        val binding = DataBindingUtil.inflate<FragmentSavedPostsBinding>(
            inflater, R.layout.fragment_saved_posts, container, false
        )
        binding.viewModel = viewModel

        binding.postListRecyclerView.layoutManager = LinearLayoutManager(context)
        viewModel.postList.observe(viewLifecycleOwner, Observer<List<Post>> {
            binding.postListRecyclerView.adapter = PostListAdapter(
                requireContext(),
                it
            ) { post ->
                SavedPostsFragmentDirections.actionSavedPostsFragmentToPostDetailsFragment(
                    post
                )
            }
        })

        binding.executePendingBindings()

        return binding.root
    }

}