package com.example.bsa_android.recyclerView

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.NavDirections
import androidx.recyclerview.widget.RecyclerView
import com.example.bsa_android.R
import com.example.bsa_android.model.Post

class PostListAdapter(
    val context: Context,
    val data: List<Post> = listOf(),
    val navAction: (post: Post) -> NavDirections
) :
    RecyclerView.Adapter<PostListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return PostListViewHolder(layoutInflater.inflate(R.layout.rv_post_view, parent, false))
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: PostListViewHolder, position: Int) {
        val item = data[position]
        holder.bind(context, item, navAction)
    }

}