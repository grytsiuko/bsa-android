package com.example.bsa_android.recyclerView

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bsa_android.R
import com.example.bsa_android.model.Post
import com.example.bsa_android.model.PostRoomDatabase
import com.example.bsa_android.repository.PostLocalRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class PostListViewHolder(val view: View) :
    RecyclerView.ViewHolder(view) {

    private val maxTextLength = 100
    private val maxTitleLength = 30

    var title: TextView = itemView.findViewById(R.id.post_title)
    var date: TextView = itemView.findViewById(R.id.post_date)
    var text: TextView = itemView.findViewById(R.id.post_text)

    fun bind(context: Context, post: Post, navAction: (post: Post) -> NavDirections) {
        fillViews(post)

        view.setOnClickListener {
            val action = navAction(post)
            view.findNavController().navigate(action)

            GlobalScope.launch {
                val localRepository =
                    PostLocalRepository(PostRoomDatabase.getDatabase(context).postDao())
                if (localRepository.getById(post.id) == null) {
                    localRepository.insert(post)
                }
            }
        }
    }

    private fun formatText(string: String, maxLength: Int) =
        if (string.length > maxLength)
            string.subSequence(0, maxLength - 3).padEnd(maxLength, '.')
        else
            string

    private fun fillViews(post: Post) {
        val viewText = formatText(post.text, maxTextLength)
        val viewTitle = formatText(post.title, maxTitleLength)

        title.text = viewTitle
        date.text = post.date
        text.text = viewText
    }

}