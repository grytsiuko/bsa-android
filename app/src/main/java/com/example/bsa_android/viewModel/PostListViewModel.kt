package com.example.bsa_android.viewModel

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.bsa_android.model.Post
import com.example.bsa_android.repository.PostLocalRepository
import com.example.bsa_android.model.PostRoomDatabase
import com.example.bsa_android.repository.PostRemoteRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.Exception

class PostListViewModel(application: Application) : AndroidViewModel(application) {
    val postList: MutableLiveData<List<Post>> = MutableLiveData(listOf())
    val loading = ObservableField(false)

    private val remoteRepository = PostRemoteRepository()
    private val localRepository = PostLocalRepository(PostRoomDatabase.getDatabase(application).postDao())

    fun refresh() {
        loading.set(true)
        GlobalScope.launch {
            try {
                postList.postValue(remoteRepository.getAll())
            } catch (e: Exception) {
                postList.postValue(localRepository.getAllNow())
            }
            loading.set(false)
        }
    }
}