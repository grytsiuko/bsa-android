package com.example.bsa_android.viewModel

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.bsa_android.model.Post
import com.example.bsa_android.repository.PostLocalRepository
import com.example.bsa_android.model.PostRoomDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class SavedPostsViewModel(application: Application) : AndroidViewModel(application) {

    val loading = ObservableField(false)

    private val repository =
        PostLocalRepository(PostRoomDatabase.getDatabase(application).postDao())
    var postList: LiveData<List<Post>> = repository.getAll()

    fun clear() {
        GlobalScope.launch { repository.deleteAll() }
    }
}