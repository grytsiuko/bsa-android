package com.example.bsa_android

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bsa_android.recyclerView.PostListAdapter
import com.example.bsa_android.databinding.FragmentPostListBinding
import com.example.bsa_android.model.Post
import com.example.bsa_android.viewModel.PostListViewModel

class PostListFragment : Fragment() {

    lateinit var viewModel: PostListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        viewModel = ViewModelProviders.of(this).get(PostListViewModel::class.java)

        val binding = DataBindingUtil.inflate<FragmentPostListBinding>(
            inflater, R.layout.fragment_post_list, container, false
        )
        binding.viewModel = viewModel

        binding.postListRecyclerView.layoutManager = LinearLayoutManager(context)
        viewModel.postList.observe(viewLifecycleOwner, Observer<List<Post>> {
            binding.postListRecyclerView.adapter = PostListAdapter(
                requireContext(),
                it
            ) { post ->
                PostListFragmentDirections.actionPostListFragmentToPostDetailsFragment(post)
            }
        })

        binding.savedButton.setOnClickListener(
            Navigation.createNavigateOnClickListener(R.id.action_postListFragment_to_savedPostsFragment)
        )

        binding.executePendingBindings()
        return binding.root
    }
}