package com.example.bsa_android

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.example.bsa_android.databinding.FragmentPostDetailsBinding
import com.example.bsa_android.model.Post

class PostDetailsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        val post = requireArguments().getParcelable<Post>("post")

        val binding = DataBindingUtil.inflate<FragmentPostDetailsBinding>(
            inflater, R.layout.fragment_post_details, container, false
        )
        binding.post = post

        return binding.root
    }
}